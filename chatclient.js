var net = require('net');
var readline = require('readline');
var prompt = require('prompt-sync')();
 
if(process.argv.length != 4){
	console.log("Usage: node %s <host> <port>", process.argv[1]);
	process.exit(1);	
}

var host=process.argv[2];
var port=process.argv[3];

if(host.length >253 || port.length >5 ){
	console.log("Invalid host or port. Try again!\nUsage: node %s <port>", process.argv[1]);
	process.exit(1);	
}

var client = new net.Socket();
console.log("--------------------------------------------------------------------------------");
console.log("Simple chatclient.js developed by Phu Phung, SecAD and modified by Antall Nguyen");
console.log("--------------------------------------------------------------------------------");
client.connect(port, host, connected);

function connected(){
	console.log("Connected to: %s:%s", client.remoteAddress, client.remotePort);
	console.log("You have to login before you can chat.");
	console.log("--------------------------------------");
	login();
}

function login(){
	var username = prompt("Username: ", {value: null});
	var password = prompt("Password: ", {value: null, echo: '*'});
	if (username == null || password == null){
		console.log("Interrupt signaled. Exiting...");
		process.exit(1);
		
	}else if((username.length > 20 || password.length > 20) || username.length < 5){
		console.log("Username needs to be more than 5 characters. Try again.");
		login();
	}
	else{
		var login_info = {
			Username: username,
			Password: password
		};
		var str_login = JSON.stringify(login_info);
		client.write(str_login);
	}
}

function menu(){
	console.log("---------------------------------------------------------------");
	console.log("List of user commands:")
	console.log(".exit - Disconnect and exit the program.")
	console.log(".users - Get list of connected users.")
	console.log(".whisper [name] [message] - Send a private message to a user.")
	console.log(".commands - Get list of user commands.")
	console.log("---------------------------------------------------------------");
}

function isEmptyOrSpaces(str){
	return str === null || str.match(/^ *$/) !== null;
}

const keyboard = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});
keyboard.on('line', input => {
	var inputArr = input.split(" ");
	var command = inputArr[0];
	if(command === ".exit"){
		client.destroy();
		console.log("Disconnected!");
		process.exit();
	}else if(command === ".users"){
		var userCmd = {
			Command: "GetUsers"
		};
		client.write(JSON.stringify(userCmd));
	}else if(command === ".whisper"){
		var name = inputArr[1];
		var msg = inputArr.slice(2).join(" ");
		if(msg.length > 250){
			console.log("<ERROR: YOUR MESSAGE IS TOO LONG.>")
		}else{
			if((name === null || name === "" ) || isEmptyOrSpaces(msg)){
				console.log("Incorrect whisper format:\n\tUsage: .whisper [name] [message]");
				console.log("\tExample: .whisper user1 This is a message!");
			}else{
				var whisper = {
					Command: "Whisper",
					User: name,
					Message: msg
				};
				client.write(JSON.stringify(whisper));
			}
		}
	}else if(command === ".commands"){
		menu();
	}else{
		var publicChat = {
			MsgType: "Public",
			Message: input
		};
		client.write(JSON.stringify(publicChat));
	}
});

client.on("data", data => {
	console.log("%s", data);
	if(data == "Invalid username or password\n"){
		console.log("Incorrect username or password. Please try again");
		login();
	}else if(data == "Login Success\n"){
		console.log("You have successfully logged in!");
		menu();
		console.log("Say Hello!");
	}
});

client.on("error", function(err){
	console.log("Error,  disconnecting...");
	process.exit(2);
});

client.on("close", function(data){
	console.log("Connection has been closed");
	process.exit(3);
});
