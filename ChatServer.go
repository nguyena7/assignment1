/* Simple EchoServer in GoLang by Phu Phung, customized by Antall Nguyen for SecAD*/
package main

import (
   "encoding/json"
	"fmt"
	"net"
	"os"
)

const BUFFERSIZE int = 1024

// -- map will be net.Conn key with type User value -- //
var allClient_conns = make(map[net.Conn]interface{})

// -- map to handle duplicate users connecting from different terminals -- //
var allUser_conns = make(map[string]int)

var newclient = make(chan User)
var lostclient = make(chan User)
type Client_Message struct{
	MsgType string
	Command string
	User string
	Message string
}

type Login struct{
	Username string
	Password string
}

type User struct{
	Username string
	Client net.Conn
	Login bool
}

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s <port>\n", os.Args[0])
		os.Exit(0)
	}
	port := os.Args[1]
	if len(port) > 5 {
		fmt.Println("Invalid port value. Try again!")
		os.Exit(1)
	}
	server, err := net.Listen("tcp", ":"+port)
	if err != nil {
		fmt.Printf("Cannot listen on port '" + port + "'!\n")
		os.Exit(2)
	} 
	fmt.Println("ChatServer in GoLang developed by Phu Phung, SecAD, revised by Antall Nguyen")
	fmt.Printf("ChatServer is listening on port '%s' ...\n", port)

	go func(){
		for{
			client_conn, _ := server.Accept()
			go login(client_conn)
		}
	}()
	for {
		select{
			case client := <-newclient:

			allClient_conns[client.Client] = client
			allUser_conns[client.Username] = 0
			go client_goroutine(client)
		}
	}
}


//-- login function --//
func login(client_conn net.Conn){
	loginmessage := fmt.Sprintf("Client '%s' just connected. Waiting for Login.\n", client_conn.RemoteAddr().String())
	fmt.Println(loginmessage)

	var buffer [BUFFERSIZE]byte

	go func(){
		username := ""
		logged_in := 0
		for logged_in != 1{
			byte_received, read_err := client_conn.Read(buffer[0:])
			if read_err != nil {
				fmt.Println("Error in receiving...")
				username = client_conn.RemoteAddr().String()
				lostclient <- User{Username: username, Client: client_conn, Login: false}
				return
			}

			client_data := buffer[0:byte_received]
			isLoggedIn, username, message := check_login(client_data)
			if isLoggedIn == true{
				fmt.Println(message)
				sendTo([]byte(message), client_conn)
				logged_in = 1
				newclient <- User{Username: username, Client: client_conn, Login: true}
			}else{
				fmt.Println(message)
				go sendTo([]byte(message), client_conn)
			}
		}
	}()

	for{
		select{
			case client := <-lostclient:
				byemessage := fmt.Sprintf("Client " + client.Username + " has disconnected." + "\n")
				delete(allClient_conns, client.Client)
				if !findSameUser(client.Username) {
					delete(allUser_conns, client.Username)
				}
				go sendtoAll([]byte(byemessage))
		}
	}
}

// -- Find clients that are conencting with same Username -- //
func findSameUser(find_user string) bool{
	for _, client := range allClient_conns{
		client := client.(User).Username
		if client == find_user{
			return true
		}
	}
	return false
}

// -- Validate login credentials -- //
func check_login(client_data []byte) (bool, string, string){
	var login Login
	fmt.Println("<SERVER DEBUG> checking login: " + string(client_data))
	err := json.Unmarshal(client_data, &login)
	if err != nil || login.Username == "" || login.Password == ""{
		fmt.Println("<SERVER> JSON Parsing Error: %s\n", err)
		return false, "", `[BAD LOGIN] Expected: {"Username": "..", "Password": ".."}\n`
	}
	fmt.Printf("<SERVER DEBUG> Login: %s\n", login)
	if check_account(login.Username, login.Password){
		return true, login.Username, "Login Success\n"
	}
	return false, "", "Invalid username or password\n"
}

// -- Function that contains valid usernames and passwords -- //
func check_account(username string, password string) bool {
	tmp_password := "password"
	name_list := []string{"antall1", "antall2", "antall3"}
	if (password == tmp_password && contains(name_list, username)){
		return true
	}
	return false
}

// -- Helper function to compare usernames and passwords with the "database" -- // 
func contains(arr []string, str string) bool{
	for _, a := range arr{
		if a == str{
			return true
		}
	}
	return false
}

// -- Manage Clients that successfully logged in -- //
func client_goroutine(client User){
	welcomemessage := fmt.Sprintf("A new client '%s' connected!\nNumber of connected clients: %d\nConnected users: \n%s", client.Username, len(allUser_conns), getUserList())
	fmt.Printf("<SERVER DEBUG> %s", welcomemessage)
	go sendtoAllExptSender([]byte (welcomemessage), client.Client)
	var buffer [BUFFERSIZE]byte
	go func(){
		for {
			byte_received, read_err := client.Client.Read(buffer[0:])
			if read_err != nil {
				fmt.Println("Error in receiving...")
				lostclient <- client
				return
			}

			client_data := buffer[0:byte_received]
			handle_data(client_data, client)
		}
	}()
	for{
		select{
			case client := <-lostclient:
				byemessage := fmt.Sprintf("Client %s has disconnected\n", client.Username)
				delete(allClient_conns, client.Client)
				if !findSameUser(client.Username) {
					delete(allUser_conns, client.Username)
				}
				go sendtoAll([]byte(byemessage))
		}
	}
}

// -- Handle received Messages -- //
func handle_data(client_data []byte, client User){
		fmt.Printf("<SERVER RECEIVED> %s\n", client_data)

		var message Client_Message
		err := json.Unmarshal(client_data, &message)
		if err != nil{
			fmt.Printf("Client provided incorrect JSON format: %s\n", err)
			sendTo([]byte("Incorrect Data format. (Messages must be of JSON format)\n"), client.Client)
			return
		}

		// Handle Private Message
		if (message.Command == "Whisper"){
			if(message.User == client.Username){
				fmt.Println("<SERVER SENDING> Cannot whisper to yourself.")
				sendTo([]byte("Cannot whisper to yourself.\n"), client.Client)
				return
			}
			if !whisperToUsers(message, client){
				sendTo([]byte("User does not exist\n"), client.Client)
			}
		// Handle Getting User list
		}else if (message.Command == "GetUsers"){
			userList := getUserList()
			fmt.Println("<SERVER SENDING> Connected Users: ", userList)
			go sendTo([]byte ("Connected Users:\n" + userList), client.Client)
		// Public chat
		}else if (message.MsgType == "Public"){
			public_msg := client.Username + ": " + message.Message
			fmt.Println("<SERVER SENDING> " + public_msg)
			go sendtoAllExptSender([]byte(public_msg), client.Client)
		}
}

// -- Send the Connected Users list to client -- //
func getUserList() string{
	var userList = ""
	for usr,_ := range allUser_conns{
		userList += ("\t" + usr + "\n")
	}
	return userList
}

// -- Private Messaging -- //
func whisperToUsers(message Client_Message, client User) bool{
		found_user := false
		for _, usr := range allClient_conns{
			usr := usr.(User)
			if usr.Username == message.User{
				found_user = true
				whisper_msg:= fmt.Sprintf("%s whispers to you: %s\n" ,client.Username, message.Message)
				fmt.Println("<SERVER SENDING>" + whisper_msg)
				sendTo([]byte(whisper_msg), usr.Client)
			}
		}
		return found_user
}

// -- Sending messages to all connected clients -- //
func sendtoAll(data []byte){
	for client_conn, _ := range allClient_conns{
		_, write_err := client_conn.Write(data)
		if write_err !=  nil{
			fmt.Println("Error in sending...")
			continue
		}
	}
}

// -- Similar to "sendToAll" but the client that sends their chat message -- //
// -- does not receive their own message -- //
func sendtoAllExptSender(data []byte, sender net.Conn){
	for client_conn, _ := range allClient_conns{
		if client_conn == sender{
			continue
		}else{
			_, write_err := client_conn.Write(data)
			if write_err !=  nil{
				fmt.Println("Error in sending...")
				continue
			}
		}
	}
}

// -- Send message to specific user -- //
func sendTo(data []byte, client_conn net.Conn){
	_, write_err := client_conn.Write(data)
	if write_err !=  nil{
		fmt.Println("Error in sending...")
		return
	}
}
